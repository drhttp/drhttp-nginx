This is the official [NGINX](https://www.nginx.com/) client for [Dr. Ashtetepe](https://drhttp.com/) service.

*Note: At the moment, as we are developing Dr. Ashtetepe, we ask you to use [Moesif](https://www.moesif.com/)'s [lua library](https://github.com/Moesif/lua-resty-moesif). Thanks to them to permit that via their [Apache licence](https://raw.githubusercontent.com/Moesif/lua-resty-moesif/master/LICENSE)*. This README is a [TL;DR](https://en.wiktionary.org/wiki/tl;dr) for Dr.Ashtetepe usage.

# Installation
> [An integration example is provided here](https://bitbucket.org/drhttp/drhttp-nginx/src/master/example/)

1) Install lua packages with [luarocks](https://luarocks.org/) (or any compatible package manager). If you don't have it we recommend you to use [OpenResty docker image](https://hub.docker.com/r/openresty/openresty):
    ```
    luarocks install luajson
    luarocks install lua-cjson
    luarocks install luasocket
    luarocks install lua-resty-moesif
    ```

2) Retrieve a `dsn` ([Data source name](https://en.wikipedia.org/wiki/Data_source_name)) which can be found in [your project](https://drhttp.com/projects). eg: `https://<my_project_api_key>@api.drhttp.com/`

3) Update your `nginx.conf` :

   1) Setup librairy in your `http` context:

        ```nginx
        http {
            ...
        
            lua_shared_dict moesif_conf 2m;
            init_by_lua_block {
                ngx.shared.moesif_conf:set("application_id", "your project api key")
                ngx.shared.moesif_conf:set("api_endpoint", "https://api.drhttp.com/moesif")
            }
            lua_package_path "/usr/local/openresty/luajit/share/lua/5.1/lua/resty/moesif/?.lua;;";
            access_by_lua '
                local req_body, res_body = "", ""
                local req_post_args = {}
                ngx.req.read_body()
                req_body = ngx.req.get_body_data()
                local content_type = ngx.req.get_headers()["content-type"]
                if content_type and string.find(content_type:lower(), "application/x-www-form-urlencoded", nil, true) then
                  req_post_args = ngx.req.get_post_args()
                end
                ngx.ctx.moesif = { req_body = req_body, res_body = res_body, req_post_args = req_post_args }
            ';
            body_filter_by_lua '
                local chunk = ngx.arg[1]
                local moesif_data = ngx.ctx.moesif or { res_body = "" }
                moesif_data.res_body = moesif_data.res_body .. chunk
                ngx.ctx.moesif = moesif_data
            ';

            ...
        }
        ```

   2) Enable capture in any `location` context you want, or even `server` wide :

        ```nginx
        log_by_lua_file /usr/local/openresty/luajit/share/lua/5.1/lua/resty/moesif/send_event.lua;
        ```
    > You can look at an example of [nginx.conf](https://bitbucket.org/drhttp/drhttp-nginx/src/master/example/nginx.conf)

### User identification

```nginx
header_filter_by_lua_block  { 
    # Optionally, identify the user such as by a header value
    ngx.var.user_id = ngx.req.get_headers()["User-Id"]
}
```

### Device identification

*Note: Device identification is not available yet in the nginx library*

## Outbound request recording

*Note: Outbound request recording is not available yet in the nginx library*

# Troubleshooting

Please [report any issue](https://bitbucket.org/drhttp/drhttp-nginx/issues/new) you encounter concerning documentation or implementation. This is very much appreciated. We'll upstream the improvements to Moesif.
